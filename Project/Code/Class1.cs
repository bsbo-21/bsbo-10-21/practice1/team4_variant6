﻿using Project1;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Project1
{
    internal class Class1
    {
        static void Main(string[] args)
        {
            ulong barcode = 8765432;
            string name = "Milk";
            int place = 1;
            int qty = 10;

            Console.WriteLine("\tДобавление");
            ProductCatalog.Recieve_Item(barcode, name, qty, place);

            Console.WriteLine("\tРезервирование");
            ProductCatalog.Reserve_Place(barcode, place);

            Console.WriteLine("\tОтгрузка");
            ProductCatalog.Sending_Item(barcode, qty);

            Console.WriteLine("\tСписание");
            ProductCatalog.Delete_Item(barcode, qty);
        }
    }

    public class Item
    {
        public ulong barcode;
        public string name;
        public int qty;
        public int place;
        public Item(ulong barcode, string name, int qty, int place)
        {

        }
    }

    public class ProductCatalog
    {
        static Item item = new Item(98765432, "hjgfd", 34, 1);
        static Store store = new Store();

        public static void Recieve_Item(ulong barcode, string name, int qty, int place)
        {
            Console.WriteLine("Класс-контроллер ProductCatalog вызывает метод add_product у класса Store");
            Item item = new Item(barcode, name, qty, place);
            store.add_product(item);
        }

        public static void Reserve_Place(ulong barcode, int place)
        {
            Console.WriteLine("Класс-контроллер ProductCatalog вызывает метод reserve_place у класса Store");
           
            item.place = place;
            store.reserve_place(barcode, place);
        }

        public static void Sending_Item(ulong barcode,int qty)
        {
            Console.WriteLine("Класс-контроллер ProductCatalog вызывает метод sending_product у класса Store");
            item.qty -= qty;
            store.sending_product(barcode, qty);
        }

        public static void Delete_Item(ulong barcode, int qty) 
        { 
            Console.WriteLine("Класс-контроллер ProductCatalog вызывает метод delete_product у класса Store");
            item.qty -= qty;
            store.delete_product(barcode, qty);
        }


    }

    public class Store
    {

        List<Item> items = new List<Item>();
        public void add_product(Item item)
        {

            items.Add(item);

            Console.WriteLine("Метод add_product принимает объект типа Item," +
                " добавляет его в каталог товаров и увеличивает количество товара на складе.");
            return;
        }


        public void reserve_place(ulong barcode, int place)
        {
            Console.WriteLine("Метод reserve_place проверяет наличие необходимого места на складе," +
                " если оно есть, то резервирует его для новых товаров, а если нет, то уведомляет об этом.");
            return;
        }

        public void sending_product(ulong barcode, int qty)
        {

            Console.WriteLine("Метод sending_product проверяет наличие запрашиваемого товара на складе. " +
                "Если товар есть, то отправляет его покупателю, удаляет из каталога товаров и уменьшает количество товара на складе. " +
                "Если товара на складе не найдено, то уведомляет об этом.");
            return;
        }

        public void delete_product(ulong barcode, int qty)
        {

            Console.WriteLine("Метод delete_product удаляет из каталога товаров и уменьшает количество товара на складе," +
            " если он был списан со склада.");
            return;
        }
    }
}


