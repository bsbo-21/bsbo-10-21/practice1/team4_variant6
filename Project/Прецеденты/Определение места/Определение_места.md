Рамки. Информационная система склада. 

Уровень. Задача, определенная пользователем.

Основной исполнитель. Кладовщик.

Заинтересованные лица и их требования.
-   Кладовщик. Замотивирован правильно организовать хранение товара на складе.
-   Система определения места. Хочет получать информацию о месте товара на складе и сохранять её.

Предусловия. Кладовщик идентифицирован и аутентифицирован.  

Результаты (Постусловия). 

Складные данные о товарах в системе внесены и сохранены.

Основной процесс.

1.  Кладовщик производит сканирование штрих-кода
2.  Система отображает свободные места на складе
2.  Кладовщик производит выбор места
2.  Система резервирует место за этим товаром


Специальные требования
-   Ноутбук с достойной автономностью.
-   Отклик системы определения места в 90% случаев приходит в течении 10 секунд.

Частота использования: периодическая

Открытые вопросы
-   Как правильно распределять место под товар на складе.